// server
const express = require('express');
const http = require('http');

// serial port
const SerialPort = require('serialport');
const ByteLength = require('@serialport/parser-byte-length');

// socketio
const SocketIo = require('socket.io');

// counter characters in buffers
const counterCharacters = 2;



const app = express();
const server = http.createServer(app);
const io = SocketIo.listen(server);


// routes
app.get('/', (req, res) => {
    res.sendFile(__dirname +'/index.html');
});


// create serial port
const port = new SerialPort('COM4', {
    baudRate: 9600
});

//option serial port
const parser = port.pipe(new ByteLength({length: counterCharacters * 4 - 1 }));

//open serial port
port.on('open', function () {
    console.log('Opened Port.');
});

//read data serial port
parser.on('data', function (data) {
    console.log(data.toString());
    io.emit('arduino:data', {
        value: data.toString()
    });
});

//error serial port
port.on('err', function (data) {
    console.log(err.message);
});

// listen server
server.listen(3000, () => {
    console.log('Server on port 3000');
});