const unsigned char size = 5;
unsigned char frame [size] = {0};

void setup()
{
    Serial.begin(9600);   
}

void loop()
{
    for (int i = 0; i < size; i++)
    {
        if ( frame [i] < 10 )
            Serial.print("00");
        else if ( frame [i] < 100 )
            Serial.print("0");
        Serial.print(frame [i]);

        if ( i == ( size - 1 ) )
            break;
        
        Serial.print(" ");
    }
    
    for (int i = 0; i < size; i++)
    {
        frame [i] = random(256);
    }

    delay(2000);
}