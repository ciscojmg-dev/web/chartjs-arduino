// server
const express = require('express');
const http = require('http');

// serial port
const SerialPort = require('serialport');
const ByteLength = require('@serialport/parser-byte-length');
const Delimiter = require('@serialport/parser-delimiter');

// socketio
const SocketIo = require('socket.io');

// counter characters in buffers
const counterCharacters = 5;

const app = express();
const server = http.createServer(app);
const io = SocketIo.listen(server);


// routes
app.get('/', (req, res) => {
    res.sendFile(__dirname +'/index.html');
});


// create serial port
const port = new SerialPort('COM7', {
    baudRate: 115200
});

//option serial port
// const parser = port.pipe(new ByteLength({length: counterCharacters * 3 - 1 }));
const parser = port.pipe(new Delimiter({ delimiter: '\n' }));

//open serial port
port.on('open', function () {
    console.log('Opened Port.');
});

//read data serial port
parser.on('data', function (data) {
    console.log(data.toString());
    io.emit('arduino:data', {
        value: data.toString()
    });
});

//error serial port
port.on('err', function (data) {
    console.log(err.message);
});

// listen server
server.listen(3000, () => {
    console.log('Server on port 3000');
});