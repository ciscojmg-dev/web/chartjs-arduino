# Chart.js + Arduino
- En cada carpeta encontraras un ejemplo de como puedes desarrollar una grafica utilizando un **Arduino y Nodejs**.

## ⚠ Requerimientos
  - Hardware
    - Arduino
    - Cable USB
  - Software
    - Node.js

## ‼ Ejecucion
  - Seleccionar la carpeta que desee ejecutar la grafica
  - Programar el arduino
    - Determinar Puerto (`COM, dev, etc`)
  - Cambiar en el archivo **`app/index.js`** por el puerto Serial donde esta conectado el Arduino.
    - Example
      - Si tu Arduino esta conectado al puerto serial en el `COM5`, el codigo quedaria de la siguiente manera

```js
// create serial port
const port = new SerialPort('COM5', {
    baudRate: 9600
});
```
  - Abre una terminal y dirigete dentro de la carpeta app/
    - Ejemplo
      - **`cd bars/app/`**
  - Dentro de la carpeta app ejecuta para instalar los paquetes
     - **`node i`**
  - Luego de estar instalados debes ejecutar
     - **`node index.js`**
  - Dirigirte en tu navegador a la siguiente direccion
    - [localhost](http://localhost:3000/)

## 💻 Ejemplo de graficos
![bar](/uploads/eb59055fb9b23b73f682f04a5c17836b/bar.gif)
![line](/uploads/ae5341cd7d34ea21c38c744a9dc23cb3/line.gif)

## ⁉ Nota:
  - Cualquier duda.
    - [ciscojmg](https://gitlab.com/ciscojmg)


